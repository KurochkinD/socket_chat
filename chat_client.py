from socket import socket, AF_INET, SOCK_STREAM
import sys
from threading import Thread
from config import ADDRESS, MAX_MESSAGE_LEN

class Client:
    def __init__(self, user=socket(AF_INET, SOCK_STREAM)):
        self.user = user
        user.connect(ADDRESS)

    def listen(self):
        while True:
            message = self.user.recv(MAX_MESSAGE_LEN).decode('utf-8')
            print(message)

    def send_message(self, message=None):
        while True:
            if not message:
                message = input('- ')

            self.user.send(message.encode('utf-8'))

            if message == '!disconnect' or '!exit' or '!leave':
                sys.exit()


if __name__ == '__main__':
    client = Client()

    tread =Thread(target=client.listen, daemon=True)
    tread.start()

    while True:
        message = input('- ')
        client.send_message(message)
