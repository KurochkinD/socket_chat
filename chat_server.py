from socket import socket, AF_INET, SOCK_STREAM

from config import ADDRESS, MAX_MESSAGE_LEN
from threading import Thread


class App:
    def __init__(self, rooms=[[None, None] for i in range(10)], server=socket(AF_INET, SOCK_STREAM)):
        socket(AF_INET, SOCK_STREAM)
        self.rooms = rooms
        self.server=server
        self.server.bind(ADDRESS)

    def find_room(self, conn) -> int:
        for id in range(len(self.rooms)):
            if None in self.rooms[id]:
                return id

    def check_is_room_filled(self, id:int) -> bool:
        if not None in self.rooms[id]:
            return True
        return False

    def join(self, conn, id:int):
        for i in range(2):
            if self.rooms[id][i]:
                self.rooms[1-i] = conn
                continue

        for user in self.rooms[id]:
            if user and user != conn:
                user.send("Start messaging with your companion".encode('utf-8'))

    def leave(self, conn, id:int):
        for i in range(2):
            if self.rooms[id][i] == conn:
                self.rooms[id][i] = None
                self.rooms[id][1-i].send(f"Your companion left the chat".encode('utf-8'))
                continue

    def send_message(self, conn, id:int, message:str):
        for i in range(2):
            if self.rooms[id][i] != conn and self.rooms[id][i]:
                self.rooms[id][i].send(f"User1: {message}".encode('utf-8'))
                print(message)
                continue

    def handle(self, conn, address):
        room_id = self.find_room(conn)
        self.join(conn, room_id)
        conn.send(f"You've joined the room {room_id}".encode('utf-8'))
        joined = True
        if self.check_is_room_filled(room_id):
            conn.send("Start messaging with your companion".encode('utf-8'))

        while joined:
            message = conn.recv(MAX_MESSAGE_LEN).decode('utf-8')
            if message == '!disconnect' or '!exit' or '!leave':
                self.leave(conn, room_id)
                joined = False
                conn.send("You've been disconnected".encode('utf-8'))

            else:
                self.send_message(conn, room_id, message)

        conn.close()

    def run(self):
        self.server.listen()
        print('Listening...')
        while True:
            conn, address = self.server.accept()
            thread = Thread(target=self.handle, args=(conn, address))
            thread.start()

if __name__ == '__main__':
    app = App()
    app.run()